package com.joyce_nhabiqueexample.equacaoquadratica.Adapters.ViewPager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.joyce_nhabiqueexample.equacaoquadratica.Views.Fragments.CalcFragment;
import com.joyce_nhabiqueexample.equacaoquadratica.Views.Fragments.HistoryFragment;

/**
 * Created by Joyce_Nhabique on 11/6/2017.
 */

public class MainAdapter extends FragmentPagerAdapter {
    private String [] titles = {"Calcs", "History"};

    public MainAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new CalcFragment();
            case 1:
                return new HistoryFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}

