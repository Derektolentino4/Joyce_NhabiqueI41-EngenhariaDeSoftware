package com.joyce_nhabiqueexample.equacaoquadratica.Views.Fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.joyce_nhabiqueexample.equacaoquadratica.R;
import com.joyce_nhabiqueexample.equacaoquadratica.ViewModel.MainViewModel;

/**
 * Created by Joyce_Nhabique on 11/6/2017.
 */

public class CalcFragment extends Fragment {

    private Button btn_calcular;
    private EditText txt_a, txt_b, txt_c;
    private MainViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calc, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_calcular = view.findViewById(R.id.btn_calc);
        txt_a = view.findViewById(R.id.txt_a);
        txt_b = view.findViewById(R.id.txt_b);
        txt_c = view.findViewById(R.id.txt_c);
        viewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);

        btn_calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(txt_a.getText().toString().isEmpty() || txt_b.getText().toString().isEmpty() || txt_c.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "Todos os campos devem estar preenchidos!", Toast.LENGTH_SHORT).show();
                    return ;
                }else{
                    double a = Double.parseDouble(txt_a.getText().toString());
                    double b = Double.parseDouble(txt_b.getText().toString());
                    double c = Double.parseDouble(txt_c.getText().toString());

                    if (a == 0){
                        Toast.makeText(getContext(), "O valor de 'a' deve ser diferente de 0", Toast.LENGTH_SHORT).show();
                    }else {
                        viewModel.calculaRaizes(a, b, c);
                        Toast.makeText(getContext(), "Calculado com sucesso", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
