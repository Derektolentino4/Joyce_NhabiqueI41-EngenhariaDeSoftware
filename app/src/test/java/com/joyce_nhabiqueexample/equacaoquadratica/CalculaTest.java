package com.joyce_nhabiqueexample.equacaoquadratica;

import com.joyce_nhabiqueexample.equacaoquadratica.ViewModel.MainViewModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Joyce_Nhabique on 11/6/2017.
 */

public class CalculaTest {
    MainViewModel viewModel;

    @Before
    public void inicializa(){
        viewModel = new MainViewModel();
    }


    // Calcula delta com valores de B e C iguais a 0;
    @Test
    public void calculaDeltaComValorBeCIguaisa0(){
        double a = 4;
        double b = 0;
        double c = 0;

        double delta = viewModel.delta(a, b, c);
        Assert.assertEquals(1.0,delta,0.000001);
    }

    //Calcula x2
    @Test
    public void calculaX2(){
        double a = 1;
        double b = 2;
        double c = 1;

        double delta = viewModel.delta(a, b, c);
        double x2 = viewModel.calculax2(a, b, c,delta);
        Assert.assertEquals(0.0,x2,0.000001);
    }

    //Calcula x1
    @Test
    public void calculaX1(){
        double a = 1;
        double b = 2;
        double c = 1;

        double delta = viewModel.delta(a, b, c);
        double x2 = viewModel.calculax2(a, b, c,delta);
        Assert.assertEquals(-1.0,x2,0.000001);
    }
}
